package de.monochromata.eclipse.eyetracking.minviable.preferences;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.monochromata.eclipse.eyetracking.minviable.MinimumViableEyeTracking;
import de.monochromata.eclipse.eyetracking.minviable.visualization.WebcamImage;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private WebcamImage webcamImage;
	private Consumer<BufferedImage> imageListener;

	public PreferencePage() {
		super(GRID);
		setPreferenceStore(MinimumViableEyeTracking.getDefault().getPreferenceStore());
		setDescription("Configuration of eye tracking in Eclipse");
	}

	@Override
	protected Control createContents(final Composite parent) {
		final Composite contents = (Composite) super.createContents(parent);
		this.webcamImage = new WebcamImage(getFieldEditorParent());
		this.imageListener = webcamImage::updateFaceImage;
		addImageListener();
		return contents;
	}

	@Override
	public void createFieldEditors() {
		// TODO: Add Clmtrackr4j configuration, e.g. viewSize selection, ...
	}

	@Override
	public void init(final IWorkbench workbench) {
	}

	@Override
	public boolean performOk() {
		removeImageListener();
		return super.performOk();
	}

	@Override
	public boolean performCancel() {
		removeImageListener();
		return super.performCancel();
	}

	@Override
	public void dispose() {
		removeImageListener(); // Just to be sure
		super.dispose();
	}

	protected void addImageListener() {
		MinimumViableEyeTracking.getDefault().addImageListener(imageListener);
	}

	protected void removeImageListener() {
		MinimumViableEyeTracking.getDefault().removeImageListener(imageListener);
	}

}