/**
 * Integration of clmtrackr4j into Eclipse.
 * 
 * TODO: Parts of this package will be replaced when a generic extension point
 * is used to obtain the eye tracker.
 */
package de.monochromata.eclipse.eyetracking.minviable;