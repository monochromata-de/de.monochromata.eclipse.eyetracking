package de.monochromata.eclipse.eyetracking.minviable;

import java.util.function.Supplier;

import de.monochromata.eyetracking.minviable.MinimumViableEyeTracker;

/**
 * A minimum-viable eye-tracker that uses a singleton {@link EclipseGazeTracking}.
 */
public class MinimumViableEclipseEyeTracker extends MinimumViableEyeTracker {

	private static EclipseGazeTracking tracker;

	/**
	 * Creates a minimum-viable eye-tracker that uses a singleton
	 * {@link EclipseGazeTracking}.
	 * <p>
	 * This constructor is used by the extension point.
	 * 
	 * @see #getSingletonTracker()
	 */
	public MinimumViableEclipseEyeTracker() {
		// TODO: Make the gazeConverter configurable via preferences
		// TODO: Add the possibility to use the mean of left and right gaze
		this(MinimumViableEclipseEyeTracker::getSingletonTracker);
	}

	public MinimumViableEclipseEyeTracker(final EclipseGazeTracking tracker) {
		this(() -> tracker);
	}

	public MinimumViableEclipseEyeTracker(final Supplier<EclipseGazeTracking> trackerSupplier) {
		super(() -> trackerSupplier.get().getGazeTracker(), (leftGaze, rightGaze) -> toPoint(rightGaze));
	}

	public static EclipseGazeTracking getSingletonTracker() {
		if (tracker == null) {
			tracker = new EclipseGazeTracking(MinimumViableEyeTracking.getDefault()::notifyImageListeners);
		}
		return tracker;
	}
}
