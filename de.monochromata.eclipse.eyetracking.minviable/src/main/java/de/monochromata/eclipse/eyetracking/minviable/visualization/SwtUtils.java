package de.monochromata.eclipse.eyetracking.minviable.visualization;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

/* 
 * JFreeChart : a free chart library for the Java(tm) platform
 * 
 *
 * (C) Copyright 2000-2007, by Object Refinery Limited and Contributors.
 *
 * Project Info:  http://www.jfree.org/jfreechart/index.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc.
 * in the United States and other countries.]
 *
 * -------------
 * SWTUtils.java
 * -------------
 * (C) Copyright 2006, 2007, by Henry Proudhon and Contributors.
 *
 * Original Author:  Henry Proudhon (henry.proudhon AT ensmp.fr);
 * Contributor(s):   Rainer Blessing;
 *                   David Gilbert (david.gilbert@object-refinery.com);
 *                   Christoph Beck.
 *
 * Changes
 * -------
 * 01-Aug-2006 : New class (HP);
 * 16-Jan-2007 : Use FontData.getHeight() instead of direct field access (RB);
 * 31-Jan-2007 : Moved the dummy JPanel from SWTGraphics2D.java,
 *               added a new convert method for mouse events (HP);
 * 12-Jul-2007 : Improved the mouse event conversion with buttons
 *               and modifiers handling, patch sent by Christoph Beck (HP);
 * 27-Aug-2007 : Modified toAwtMouseEvent signature (HP);
 * 27-Nov-2007 : Moved convertToSWT() method from SWTGraphics2D and added
 *               convertAWTImageToSWT() (DG);
 * 01-Jul-2008 : Simplify AWT/SWT font style conversions (HP);
 *
 */

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ComponentColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

/**
 * Utility class gathering some useful and general method. Mainly convert forth
 * and back graphical stuff between awt and swt.
 * <p>
 * Based on a copy from http://www.java2s.com/Tutorial/Java/0280__SWT/
 * ConvertsabufferedimagetoSWTImageData.htm
 */
public class SwtUtils {

	/**
	 * Converts an AWT image to SWT.
	 *
	 * @param image the image (<code>null</code> not permitted).
	 *
	 * @return Image data.
	 */
	public static ImageData convertAWTImageToSWT(final Image image) {
		if (image == null) {
			throw new IllegalArgumentException("Null 'image' argument.");
		}
		final int w = image.getWidth(null);
		final int h = image.getHeight(null);
		if (w == -1 || h == -1) {
			return null;
		}
		final BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		final Graphics g = bi.getGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
		return convertToSWT(bi);
	}

	/**
	 * Converts a buffered image to SWT <code>ImageData</code>.
	 *
	 * @param input the buffered image (<code>null</code> not permitted).
	 *
	 * @return The image data.
	 */
	public static ImageData convertToSWT(final BufferedImage input) {
		final BufferedImage bufferedImage = convertCustomTypeComponentColorModelToDirectColorModel(input);
		if (bufferedImage.getColorModel() instanceof DirectColorModel && bufferedImage.getType() == TYPE_INT_ARGB) {
			final DirectColorModel colorModel = (DirectColorModel) bufferedImage.getColorModel();
			final PaletteData palette = new PaletteData(colorModel.getRedMask(), colorModel.getGreenMask(),
					colorModel.getBlueMask());
			final ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
					colorModel.getPixelSize(), palette);
			final WritableRaster raster = bufferedImage.getRaster();
			final int[] pixelArray = new int[4];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, pixelArray);
					// Ignore alpha in pixelArray[4] - the alpha component
					// always follows the last color component as per the
					// JavaDoc of ColorModel.getComponentSize(int)
					final int pixel = palette.getPixel(new RGB(pixelArray[0], pixelArray[1], pixelArray[2]));
					data.setPixel(x, y, pixel);
				}
			}
			return data;
		} else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
			final IndexColorModel colorModel = (IndexColorModel) bufferedImage.getColorModel();
			final int size = colorModel.getMapSize();
			final byte[] reds = new byte[size];
			final byte[] greens = new byte[size];
			final byte[] blues = new byte[size];
			colorModel.getReds(reds);
			colorModel.getGreens(greens);
			colorModel.getBlues(blues);
			final RGB[] rgbs = new RGB[size];
			for (int i = 0; i < rgbs.length; i++) {
				rgbs[i] = new RGB(reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF);
			}
			final PaletteData palette = new PaletteData(rgbs);
			final ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
					colorModel.getPixelSize(), palette);
			data.transparentPixel = colorModel.getTransparentPixel();
			final WritableRaster raster = bufferedImage.getRaster();
			final int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, pixelArray);
					data.setPixel(x, y, pixelArray[0]);
				}
			}
			return data;
		} else if (bufferedImage.getColorModel() instanceof ComponentColorModel
				&& bufferedImage.getType() == TYPE_INT_ARGB) {
			// Based on https://stackoverflow.com/a/8385412
			final ComponentColorModel colorModel = (ComponentColorModel) bufferedImage.getColorModel();
			final PaletteData palette = new PaletteData(0xFF0000, 0x00FF00, 0x0000FF);
			final ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(),
					colorModel.getPixelSize(), palette);

			// Ignore alpha
			data.transparentPixel = -1;

			final WritableRaster raster = bufferedImage.getRaster();
			final int[] argb = new int[4];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					raster.getPixel(x, y, argb);
					// Ignore alpha in argb[0]
					final int pixel = palette.getPixel(new RGB(argb[1], argb[2], argb[3]));
					data.setPixel(x, y, pixel);
				}
			}
			return data;
		}
		return null;
	}

	private static BufferedImage convertCustomTypeComponentColorModelToDirectColorModel(final BufferedImage input) {
		if (input.getColorModel() instanceof ComponentColorModel && input.getType() == BufferedImage.TYPE_CUSTOM) {
			return convert(input, TYPE_INT_ARGB);
		}
		return input;
	}

	private static BufferedImage convert(final BufferedImage input, final int targetType) {
		final BufferedImage converted = new BufferedImage(input.getWidth(), input.getHeight(), targetType);
		new ColorConvertOp(null).filter(input, converted);
		return converted;
	}

}
