package de.monochromata.eclipse.eyetracking.minviable;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.monochromata.eclipse.eyetracking.rfl.strategy.DoubleClickSelectionStrategy;

/**
 * The activator class controls the plug-in life cycle.
 */
public class MinimumViableEyeTracking extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.monochromata.eclipse.eyetracking.clmtrackr4j"; //$NON-NLS-1$

	// The shared instance
	private static MinimumViableEyeTracking plugin;

	private final List<Consumer<BufferedImage>> imageListeners = new ArrayList<>();

	private DoubleClickSelectionStrategy doubleClickStrategy;

	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		/*
		 * // TODO: Remove - only for development final Display display =
		 * PlatformUI.getWorkbench().getDisplay(); doubleClickStrategy = new
		 * DoubleClickSelectionStrategy();
		 * doubleClickStrategy.install(singletonList(display));
		 */
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public void addImageListener(final Consumer<BufferedImage> imageListener) {
		imageListeners.add(imageListener);
	}

	public void removeImageListener(final Consumer<BufferedImage> imageListener) {
		imageListeners.remove(imageListener);
	}

	protected void notifyImageListeners(final BufferedImage image) {
		imageListeners.forEach(listener -> listener.accept(image));
	}

	public static MinimumViableEyeTracking getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

}
