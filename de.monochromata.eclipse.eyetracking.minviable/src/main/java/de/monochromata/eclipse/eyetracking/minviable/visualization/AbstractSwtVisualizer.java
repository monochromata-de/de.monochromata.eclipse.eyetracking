package de.monochromata.eclipse.eyetracking.minviable.visualization;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Consumer;

import de.monochromata.eyetracking.minviable.visualization.GraphicsPainter;
import de.monochromata.eyetracking.minviable.visualization.Painter;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

public abstract class AbstractSwtVisualizer implements Visualizer {

	private final Consumer<BufferedImage> imageRenderer;
	private final Painter painter;
	protected final Map<BufferedImage, BufferedImage> imageCopies = new WeakHashMap<>();
	private Rectangle lastFaceBounds;

	public AbstractSwtVisualizer(final Consumer<BufferedImage> imageRenderer) {
		this(imageRenderer, new GraphicsPainter());
	}

	public AbstractSwtVisualizer(final Consumer<BufferedImage> imageRenderer, final Painter painter) {
		this.imageRenderer = imageRenderer;
		this.painter = painter;
	}

	@Override
	public void initialize(final BufferedImage initialImage) {
		// Unlike
		// de.monochromata.eyetracking.clmtrackr4j.visualization.AbstractSwingVisualizer,
		// this class does not register a change listener to add further images.
		imageRenderer.accept(copy(initialImage));
	}

	@Override
	public void faceFound(final BufferedImage image, final Rectangle faceBounds) {
		lastFaceBounds = faceBounds;
		renderImageCopy(image, copy -> drawFaceBounds(copy, faceBounds));
	}

	@Override
	public void eyesFound(final BufferedImage image, final List<Point2D> facePoints) {
		renderImageCopy(image, copy -> {
			drawFaceBounds(copy, lastFaceBounds);
			drawOnImage(copy, facePoints);
		});
	}

	protected void renderImageCopy(final BufferedImage image, final Consumer<BufferedImage> drawOperation) {
		final BufferedImage target = copy(image);
		drawOperation.accept(target);
		imageRenderer.accept(target);
	}

	@Override
	public void noEyesFound(final BufferedImage image) {
		imageRenderer.accept(copy(image));
	}

	private void drawFaceBounds(final BufferedImage target, final Rectangle faceBounds) {
		if (faceBounds != null) {
			painter.drawRect(target.createGraphics(), faceBounds);
		}
	}

	protected void drawOnImage(final BufferedImage image, final List<Point2D> facePoints) {
		painter.drawFace(image, facePoints);
	}

	protected BufferedImage copy(final BufferedImage original) {
		final ColorModel colorModel = original.getColorModel();
		final BufferedImage copy = new BufferedImage(colorModel, original.copyData(null),
				colorModel.isAlphaPremultiplied(), null);
		imageCopies.put(original, copy);
		return copy;
	}

}
