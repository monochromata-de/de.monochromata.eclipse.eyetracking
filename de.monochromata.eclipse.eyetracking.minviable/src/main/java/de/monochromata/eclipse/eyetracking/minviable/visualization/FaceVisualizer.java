package de.monochromata.eclipse.eyetracking.minviable.visualization;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import de.monochromata.eyetracking.minviable.visualization.Painter;

public class FaceVisualizer extends AbstractSwtVisualizer {

	public FaceVisualizer(final Consumer<BufferedImage> imageRenderer) {
		super(imageRenderer);
	}

	public FaceVisualizer(final Consumer<BufferedImage> imageRenderer, final Painter painter) {
		super(imageRenderer, painter);
	}

}
