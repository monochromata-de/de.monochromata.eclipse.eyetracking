package de.monochromata.eclipse.eyetracking.minviable;

import static java.lang.Math.round;
import static java.util.Optional.ofNullable;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.clmtrackr4j.js.graal.GraalBackend;
import de.monochromata.eclipse.eyetracking.minviable.visualization.FaceVisualizer;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction;
import de.monochromata.eyetracking.minviable.features.BinocularFeatureExtractor;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;
import de.monochromata.eyetracking.webcam.calibration.Calibration;
import de.monochromata.eyetracking.webcam.calibration.ContinuousCalibration;
import de.monochromata.eyetracking.webcam.calibration.EyeValidationResult;
import de.monochromata.eyetracking.webcam.calibration.ValidationResult;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;
import de.monochromata.eyetracking.webcam.svm.Gaze;
import de.monochromata.eyetracking.webcam.svm.GazeTracker;
import de.monochromata.eyetracking.webcam.tools.CaptureOnClick;
import libsvm.svm_parameter;

public class EclipseGazeTracking {

    private static final int RADIUS_PX = 5;
    private static final int DIAMETER_PX = 2 * RADIUS_PX;

    private final ForkJoinPool executor = new ForkJoinPool(1);
    // TODO: Make view size configurable via preferences
    private final SarxosWebcam webcam = new SarxosWebcam(1);

    private final FeatureExtractor<BufferedImage> featureExtractor;
    private final svm_parameter hyperParameters = createHyperParameters();
    private final Calibration calibration;
    private final GraalBackend backend = new GraalBackend();
    private final Visualizer visualizer;

    private final Function<List<Point2D>, Rectangle[]> boundsFunction = positions -> EyePatchesExtraction
            .applyPadding(BoundsFunctions.getEyeBoundsFromEyeCorners(positions), 2);
    private final Function<BufferedImage, EyePatches> eyePatchDetector;
    private final int trainingThreshold = 15;
    private final int validationThreshold = 5;
    private final ContinuousCalibration<BufferedImage> continuousCalibration;

    private final AtomicReference<Consumer<Point>> manualContextConnection = new AtomicReference<>();
    private final CaptureOnClick captureOnClick = new CaptureOnClick(manualContextConnection::set, webcam);

    private final GazeTracker gazeTracker;

    private Color red, green;

    private Gaze leftGaze;
    private Gaze rightGaze;

    public EclipseGazeTracking(final Consumer<BufferedImage> imagePropagator) {
        visualizer = new FaceVisualizer(imagePropagator);
        eyePatchDetector = new EyePatchDetector(new ClmtrackrPositionsDetector(backend, visualizer), boundsFunction);
        featureExtractor = BinocularFeatureExtractor.ofImages(eyePatchDetector, 0);
        calibration = new Calibration(featureExtractor, hyperParameters);
        continuousCalibration = new ContinuousCalibration<>(executor, calibration, featureExtractor, trainingThreshold,
                validationThreshold);
        gazeTracker = new GazeTracker(webcam, featureExtractor);
        PlatformUI.getWorkbench().getDisplay().syncExec(this::initialize);
    }

    public void initialize() {
        final Control control = getControlOfActiveEditor();
        captureOnClick.addListenerForImagesWithManualContext(
                (image, point) -> continuousCalibration.addDatum(image, point.getX(), point.getY()));
        addMouseListener(control);

        continuousCalibration.addValidationListener(gazeTracker::setPredictors);
        continuousCalibration.addValidationListener(EclipseGazeTracking::logRevalidation);

        addPaintListener(control);
        addGazeListener(control);
        gazeTracker.start();
    }

    public void stop() {
        gazeTracker.stop();
        executor.shutdown();
    }

    public GazeTracker getGazeTracker() {
        return gazeTracker;
    }

    protected void addMouseListener(final Control control) {
        // TODO: Be able to remove the mouse listener again
        final MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseDown(final MouseEvent e) {
                if (!(e.widget instanceof Control)) {
                    // TODO: How to get the display-relative coordinate of a
                    // widget?
                    System.err.println(this.getClass().getName() + "...mouseDown: ignoring mouse event on widget "
                            + e.widget.getClass().getName());
                    return;
                }
                final Control control = (Control) e.widget;
                final org.eclipse.swt.graphics.Point displayRelative = control.toDisplay(e.x, e.y);
                final Point point = new PointImpl(displayRelative.x, displayRelative.y);
                System.err.println("Add training datum: " + point);
                ofNullable(manualContextConnection.get()).ifPresent(consumer -> consumer.accept(point));
            }
        };
        control.addMouseListener(mouseAdapter);
    }

    public void addGazeListener(final BiConsumer<Gaze, Gaze> gazeListener) {
        gazeTracker.addListener(gazeListener);
    }

    public void removeGazeListener(final BiConsumer<Gaze, Gaze> gazeListener) {
        gazeTracker.removeListener(gazeListener);
    }

    protected void addGazeListener(final Control control) {
        gazeTracker.addListener((leftGaze, rightGaze) -> {
            if (control.isDisposed()) {
                return;
            }
            this.leftGaze = fromDisplayToControl(leftGaze, control);
            this.rightGaze = fromDisplayToControl(rightGaze, control);
            // TODO: Specify bounds that need to be re-drawn that given both
            // the old and the new gaze points
            if (!control.isDisposed()) {
                control.getDisplay().asyncExec(control::redraw);
            }
        });
    }

    private Gaze fromDisplayToControl(final Gaze gaze, final Control control) {
        final AtomicReference<org.eclipse.swt.graphics.Point> displayRelative = new AtomicReference<>();
        control.getDisplay().syncExec(() -> {
            displayRelative.set(control.toControl((int) round(gaze.getXEstimate()), (int) round(gaze.getYEstimate())));
        });
        return new Gaze(gaze.getEyeFeatures(), displayRelative.get().x, displayRelative.get().y);
    }

    // TODO: Move gaze painting to d.m.e.e
    protected void addPaintListener(final Control control) {
        // TODO: The paint listener should be removable
        control.addPaintListener(this::paintGaze);
    }

    protected void paintGaze(final PaintEvent event) {
        paintGazeIfNotNull(leftGaze, event.gc, getRed(event.display));
        paintGazeIfNotNull(rightGaze, event.gc, getGreen(event.display));
    }

    protected void paintGazeIfNotNull(final Gaze gaze, final GC gc, final Color color) {
        if (gaze == null) {
            return;
        }
        paintGaze(gaze, gc, color);
    }

    protected void paintGaze(final Gaze gaze, final GC gc, final Color color) {
        final int x = (int) round(gaze.getXEstimate()) - RADIUS_PX;
        final int y = (int) round(gaze.getYEstimate()) - RADIUS_PX;
        final Color oldBackground = gc.getBackground();
        gc.setBackground(color);
        gc.fillOval(x, y, DIAMETER_PX, DIAMETER_PX);
        gc.setBackground(oldBackground);
    }

    protected Color getRed(final Device device) {
        return getColor(() -> this.red, color -> {
            this.red = color;
        }, new RGB(180, 0, 0), device);
    }

    protected Color getGreen(final Device device) {
        return getColor(() -> this.green, color -> {
            this.green = color;
        }, new RGB(0, 180, 0), device);
    }

    protected Color getColor(final Supplier<Color> supplier, final Consumer<Color> consumer, final RGB rgb,
            final Device device) {
        if (supplier.get() == null) {
            consumer.accept(new Color(device, rgb));
        }
        return supplier.get();
    }

    protected Control getControlOfActiveEditor() {
        // TODO: Add
        // PlatformUI.getWorkbench().getActiveWorkbenchWindow().addPageListener(listener);
        // TODO: Add
        // PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().addPartListener(listener);
        // TODO: Prevent the NullPointerExceptions if the workbench is not open,
        // etc.
        final IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                .getActiveEditor();
        final Control control = activeEditor.getAdapter(Control.class);
        System.err.println("control=" + control);
        /*
         * if(activeEditor instanceof JavaEditor) { final ISourceViewer viewer =
         * ((JavaEditor)activeEditor).getViewer(); if(viewer instanceof Viewer) {
         * ((Viewer)viewer).getControl() } }
         */
        return control;
    }

    protected static void logRevalidation(final ValidationResult validationResult) {
        logRevalidation("left", validationResult.getLeftEyeResult());
        logRevalidation("right", validationResult.getRightEyeResult());
        System.err.println();
    }

    private static void logRevalidation(final String side, final EyeValidationResult result) {
        System.err.print(side + " eye error x=" + result.getErrorX() + " y=" + result.getErrorY() + " ");
    }

    protected static svm_parameter createHyperParameters() {
        return createSamplePolynomialHyperParameters();
    }

    protected static svm_parameter createSamplePolynomialHyperParameters() {
        final svm_parameter hyperParameters = new svm_parameter();
        hyperParameters.svm_type = svm_parameter.NU_SVR;
        hyperParameters.kernel_type = svm_parameter.POLY;
        // for polynomial
        hyperParameters.degree = 3;
        hyperParameters.gamma = 0.01;
        hyperParameters.coef0 = 1;
        // for Nu
        hyperParameters.C = 1;
        hyperParameters.nu = 1;
        // technical
        hyperParameters.cache_size = 100;
        hyperParameters.eps = 0.001;
        hyperParameters.shrinking = 1;
        hyperParameters.probability = 0;
        return hyperParameters;
    }

}
