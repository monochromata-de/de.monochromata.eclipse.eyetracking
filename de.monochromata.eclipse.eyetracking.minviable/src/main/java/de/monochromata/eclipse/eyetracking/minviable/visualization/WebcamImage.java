package de.monochromata.eclipse.eyetracking.minviable.visualization;

import static de.monochromata.eclipse.eyetracking.minviable.visualization.SwtUtils.convertToSWT;

import java.awt.image.BufferedImage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * Adds labels to a given container to display webcam images.
 */
public class WebcamImage {

	private final Label imageLabel;

	public WebcamImage(final Composite container) {
		createHeadingLabel(container);
		this.imageLabel = createImageLabel(container);
	}

	protected static Label createHeadingLabel(final Composite container) {
		final Label label = new Label(container, SWT.NULL);
		label.setText("Clmtrackr image");
		return label;
	}

	protected static Label createImageLabel(final Composite container) {
		return new Label(container, SWT.NULL);
	}

	public void updateFaceImage(final BufferedImage image) {
		imageLabel.getDisplay().asyncExec(() -> updateFaceImageInUiThread(image));
	}

	protected void updateFaceImageInUiThread(final BufferedImage image) {
		if (imageLabel.isDisposed()) {
			return;
		}
		final Image oldImage = imageLabel.getImage();
		imageLabel.setImage(new Image(imageLabel.getDisplay(), convertToSWT(image)));
		imageLabel.pack();
		imageLabel.redraw();
		if (oldImage != null) {
			oldImage.dispose();
		}
	}

}
