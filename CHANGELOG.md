# Release 2.0.58

* Update eclipse.version to 2020-12
* Add parent project meta-data
* Publish to m2 and p2repo to GitLib Pages
* Replace J2V8Backend by GraalBackend
* Update assertj dependency
* Remove removed plugins from feature.xml
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.173
* Update dependency de.monochromata.event:aggregator to 1.2.77
* Update dependency de.monochromata.event:aggregator to 1.2.76
* Update dependency de.monochromata.event:aggregator to 1.2.75
* Update dependency de.monochromata.event:aggregator to 1.2.74
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.172
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.171
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.170
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.169
* Update dependency de.monochromata.event:aggregator to 1.2.73
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.168
* Update dependency de.monochromata.event:aggregator to 1.2.72
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.167
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.166
* Update dependency de.monochromata.event:aggregator to 1.2.71
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.165
* Update dependency de.monochromata.event:aggregator to 1.2.70
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.164
* Update dependency de.monochromata.event:aggregator to 1.2.69
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.163
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.162
* Update dependency de.monochromata.event:aggregator to 1.2.68
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.161
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.160
* Update dependency de.monochromata.event:aggregator to 1.2.67
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.159
* Update dependency de.monochromata.event:aggregator to 1.2.66
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.158
* Update dependency de.monochromata.event:aggregator to 1.2.65
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.157
* Update dependency de.monochromata.event:aggregator to 1.2.64
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.156
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.155
* Update dependency de.monochromata.event:aggregator to 1.2.63
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.154
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.153
* Update dependency de.monochromata.event:aggregator to 1.2.62
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.152
* Update dependency de.monochromata.event:aggregator to 1.2.61
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.151
* Update dependency de.monochromata.event:aggregator to 1.2.60
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.150
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.149
* Update dependency de.monochromata.event:aggregator to 1.2.59
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.148
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.147
* Update dependency de.monochromata.event:aggregator to 1.2.58
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.146
* Update dependency de.monochromata.event:aggregator to 1.2.57
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.145
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.144
* Update dependency de.monochromata.event:aggregator to 1.2.56
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.143
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.142
* Update dependency de.monochromata.event:aggregator to 1.2.55
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.141
* Update dependency de.monochromata.event:aggregator to 1.2.54
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.140
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.139
* Update dependency de.monochromata.event:aggregator to 1.2.53
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.138
* Update dependency de.monochromata.event:aggregator to 1.2.52
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.137
* Update dependency de.monochromata.event:aggregator to 1.2.51
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.136
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.135
* Update dependency de.monochromata.event:aggregator to 1.2.50
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.134
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.133
* Update dependency de.monochromata.event:aggregator to 1.2.49
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.132
* Update dependency de.monochromata.event:aggregator to 1.2.48
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.131
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.130
* Update dependency de.monochromata.event:aggregator to 1.2.47
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.129
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.128
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.127
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.126
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.125
* Update dependency de.monochromata.event:aggregator to 1.2.46
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.124
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.123
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.122
* Update dependency de.monochromata.event:aggregator to 1.2.45
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.121
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.120
* Update dependency de.monochromata.event:aggregator to 1.2.44
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.119
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.118
* Update dependency de.monochromata.event:aggregator to 1.2.43
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.117
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.116
* Update dependency de.monochromata.eyetracking:aggregator to 2.3.115

# Release 2.0.57

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.78
* Merge version numbers

# Release 2.0.56

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.77
* Update dependency de.monochromata.event:aggregator to 1.2.31

# Release 2.0.55

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.76
* Merge version numbers

# Release 2.0.54

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.75
* Update dependency de.monochromata.event:aggregator to 1.2.30

# Release 2.0.53

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.74

# Release 2.0.52

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.73

# Release 2.0.51

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.72

# Release 2.0.50

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.71

# Release 2.0.49

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.70

# Release 2.0.48

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.69

# Release 2.0.47

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.68

# Release 2.0.46

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.67
* Merge version numbers

# Release 2.0.45

* Update dependency de.monochromata.event:aggregator to 1.2.29

# Release 2.0.44

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.65
* Merge version numbers

# Release 2.0.43

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.64
* Update dependency de.monochromata.event:aggregator to 1.2.28

# Release 2.0.42

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.63

# Release 2.0.41

* Update dependency de.monochromata.eyetracking:aggregator to 2.3.62

# Release 2.0.40

* No changes

# Release 2.0.39

* No changes

# Release 2.0.38

* No changes

# Release 2.0.37

* No changes

# Release 2.0.36

* No changes

# Release 2.0.35

* No changes

# Release 2.0.34

* No changes

# Release 2.0.33

* No changes

# Release 2.0.32

* No changes

# Release 2.0.31

* No changes

# Release 2.0.30

* No changes

# Release 2.0.29

* No changes

# Release 2.0.28

* No changes

# Release 2.0.27

* No changes

# Release 2.0.26

* No changes

# Release 2.0.25

* No changes

# Release 2.0.24

* No changes

# Release 2.0.23

* No changes

