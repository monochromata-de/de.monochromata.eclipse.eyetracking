package de.monochromata.eclipse.eyetracking.rfl.strategy;

import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyTestUtils.keyStroke;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyTestUtils.mouseDown;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyTestUtils.mouseUp;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyTestUtils.moveMouse;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.displayRelativeBounds;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.syncExec;
import static java.util.Collections.singleton;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.function.Consumer;
import java.util.function.Function;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotView;
import org.eclipse.swtbot.swt.finder.keyboard.Keystrokes;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.time.Clock;
import de.monochromata.eyetracking.time.SystemClock;

public class MenuDetectStrategyTest extends AbstractManualContextCollectionStrategyTest {

    // TODO: Is mere right-clicking a valid RFL/PFL? Even in an editor it is
    // possible to right-click somewhere in a large whitespace
    // TODO: Does not work in Gitlab CI, most likely because there is not keyboard,
    // see
    // https://gitlab.com/de.monochromata/de.monochromata.eclipse.eyetracking/issues/27
    @Ignore
    @Test
    public void manualContextIsDetectedForRightMouseClickInPackageExplorer() {
        final Control packageExplorerControl = getPackageExplorerControl();
        final Rectangle displayRelativePackageExplorerBounds = syncExec(getBot().getDisplay(),
                () -> displayRelativeBounds(getBot().getDisplay(), packageExplorerControl));
        final int x = displayRelativePackageExplorerBounds.x;
        final int y = displayRelativePackageExplorerBounds.y;
        assertManualContextCreated(x, y, () -> openContextMenuViaRightClick(packageExplorerControl));
    }

    @Ignore
    @Test
    public void noManualContextIsDetectedForContextMenuOpenedViaKeyboardInPackageExplorer() {
        fail("assertNoManualContextCreated(() -> openContextMenuViaShiftF10());");
    }

    @Ignore
    @Test
    public void manualContextIsDetectedForRightMouseClickOnTreeItem() {
        fail("Not yet implemented");
    }

    // TODO: Shift+F10 or Ctrl+Shift+F10 are used to trigger the context menu:
    // https://en.wikipedia.org/wiki/Menu_key

    @Ignore
    @Test
    public void manualContextIsDetectedForContextMenuOpenedViaKeyboardWhenEditorIsFocused() {
        fail("Not yet implemented");
        // TODO: The location of the manual context should be equal to the
        // current cursor position in the editor or a word should be selected
    }

    // TODO: Detect a ManualContext when the keyboard is used to open a context
    // menu on a TreeItem?

    // TODO: Detect a ManualContext when a TreeItem is collapsed/expanded via
    // MouseClick/Keyboard (not via the expand/collapse all button)

    protected void assertManualContextCreated(final int expectedX, final int expectedY, final Runnable menuOpener) {
        final long fixedTimeMs = System.currentTimeMillis();
        final Clock mockClock = () -> fixedTimeMs;
        final Consumer<Consumer<Point>> verifier = listener -> {
        		verify(listener, times(1)).accept(eq(new PointImpl(expectedX, expectedY)));
        		verifyNoMoreInteractions(listener);
            };
        assertManualContext(mockClock, verifier, menuOpener);
    }

    protected void assertNoManualContextCreated(final Runnable menuOpener) {
        assertManualContext(new SystemClock(), unused -> {}, menuOpener);
    }

    @SuppressWarnings("unchecked")
    protected void assertManualContext(final Clock clock,
            final Consumer<Consumer<Point>> verifier, final Runnable menuOpener) {
        final Consumer<Point> listener = mock(Consumer.class);
        final MenuDetectStrategy strategy = new MenuDetectStrategy(clock);

        try {
            syncExec(getBot().getDisplay(), () -> {
                strategy.install(singleton(getBot().getDisplay()));
                strategy.addListener(listener);
                menuOpener.run();
                verifier.accept(listener);
                return null;
            });
        } finally {
            syncExec(getBot().getDisplay(), () -> {
                strategy.removeListener(listener);
                strategy.uninstall();
                return null;
            });
        }
    }

    protected void openContextMenuViaRightClick(final Control control) {
        clickRightMouseButtonToOpenTheContextMenu(control);
        closeTheContextMenuByPressingEscape();
    }

    protected void clickRightMouseButtonToOpenTheContextMenu(final Control control) {
        final Rectangle displayRelativeBounds = displayRelativeBounds(getBot().getDisplay(), control);
        final int x = displayRelativeBounds.x;// + 10;
        final int y = displayRelativeBounds.y;// + 10;
        moveMouse(getBot().activeShell(), control, x, y);
        mouseDown(getBot().activeShell(), control, x, y, 3);
        mouseUp(getBot().activeShell(), control, x, y, 3);
    }

    protected Control getPackageExplorerControl() {
        final SWTBotView packageExplorer = getBot().viewByTitle("Package Explorer");
        final Control packageExplorerControl = (Control) packageExplorer.getWidget();
        return packageExplorerControl;
    }

    protected void closeTheContextMenuByPressingEscape() {
        keyStroke(getBot().activeShell(), Keystrokes.ESC);
    }

}
