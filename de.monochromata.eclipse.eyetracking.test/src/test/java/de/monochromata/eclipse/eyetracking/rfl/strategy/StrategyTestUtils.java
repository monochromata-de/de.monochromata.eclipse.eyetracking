package de.monochromata.eclipse.eyetracking.rfl.strategy;

import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.accessMethod;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.syncExec;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Consumer;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.keyboard.Keyboard;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;

public class StrategyTestUtils {

	public static void keyStroke(final AbstractSWTBot<?> botWidget, final KeyStroke keyStroke) {
		useKeyboard(botWidget, keyboard -> keyboard.pressShortcut(keyStroke));
	}

	public static void typeCharacter(final AbstractSWTBot<?> botWidget, final char c) {
		useKeyboard(botWidget, keyboard -> keyboard.typeCharacter(c));
	}

	public static void useKeyboard(final AbstractSWTBot<?> botWidget, final Consumer<Keyboard> keyboardUser) {
		syncExec(((Widget) botWidget.widget).getDisplay(), () -> {
			final Keyboard keyboard = (Keyboard) accessMethod(AbstractSWTBot.class, "keyboard").invoke(botWidget);
			keyboardUser.accept(keyboard);
			return null;
		});
	}

	public static void moveMouse(final AbstractSWTBot<?> botWidget, final Widget widgetAtTargetMousePosition,
			final int x, final int y) {
		postMouseEvent(botWidget, widgetAtTargetMousePosition, SWT.MouseMove, x, y, 0);
	}

	public static void mouseDown(final AbstractSWTBot<?> botWidget, final Widget widgetAtGivenLocation, final int x,
			final int y, final int mouseButton) {
		postMouseEvent(botWidget, widgetAtGivenLocation, SWT.MouseDown, x, y, mouseButton);
	}

	public static void mouseUp(final AbstractSWTBot<?> botWidget, final Widget widgetAtGivenLocation, final int x,
			final int y, final int mouseButton) {
		postMouseEvent(botWidget, widgetAtGivenLocation, SWT.MouseUp, x, y, mouseButton);
	}

	public static void postMouseEvent(final AbstractSWTBot<?> botWidget, final Widget widgetAtGivenLocation,
			final int eventType, final int x, final int y, final int mouseButton) {
		final Display display = ((Widget) botWidget.widget).getDisplay();
		syncExec(display, () -> {
			final Event event = (Event) accessMethod(AbstractSWTBot.class, "createMouseEvent", int.class, int.class,
					int.class, int.class, int.class).invoke(botWidget, x, y, mouseButton, 0, 0);
			event.widget = widgetAtGivenLocation;
			event.type = eventType;
			assertThat(display.post(event)).isEqualTo(true);
			return null;
		});
	}

}
