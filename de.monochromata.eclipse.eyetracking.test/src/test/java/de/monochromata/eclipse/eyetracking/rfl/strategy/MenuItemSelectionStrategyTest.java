package de.monochromata.eclipse.eyetracking.rfl.strategy;

import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.getBounds;
import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.syncExec;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.function.Consumer;

import org.eclipse.swt.widgets.Menu;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.junit.Ignore;
import org.junit.Test;

import de.monochromata.eyetracking.Point;

public class MenuItemSelectionStrategyTest extends AbstractManualContextCollectionStrategyTest {

	@SuppressWarnings("unchecked")
	@Ignore
	@Test
	public void test() throws Exception {
		final Consumer<Point> listener = mock(Consumer.class);
		final MenuItemSelectionStrategy strategy = new MenuItemSelectionStrategy();

		try {

			// TODO: Fake the entire MouseUp event inkl. the widget, coordinates
			// etc.

			// TODO: install the strategy?
			strategy.addListener(listener);
			// TODO: This actually clicks a ToolBarItem, not a MenuItem
			// TODO: Also simulate right and left mouse button
			// moveMouseToTopToShowWindowMenu();
			System.err.println("mouse moved");
			// TODO: Warum wird Rectangle {-1, -1, 1, 1} zurückgegeben?
			final SWTBotMenu windowMenu = getBot().menu("Window");
			windowMenu.setFocus();
			final Menu parentMenu = syncExec(getBot().getDisplay(), () -> windowMenu.widget.getParent());
			System.err.println("Menu " + parentMenu + " is at " + getBounds(parentMenu));
			System.err.println("Window menu " + windowMenu + " is at " + getBounds(windowMenu.widget));

			// final SWTBotMenu preferencesMenu =
			// windowMenu.menu("Preferences");
			// preferencesMenu.setFocus();
			// preferencesMenu.widget;
			// preferencesMenu.click();
		} finally {
			strategy.removeListener(listener);
			// TODO: Uninstall the strategy?
			verify(listener, times(1)).accept(notNull());
			verifyNoMoreInteractions(listener);
		}
	}

	/*
	 * protected void moveMouseToTopToShowWindowMenu() {
	 * moveMouse(getBot().menu("Window"), 400, 70); }
	 */

}
