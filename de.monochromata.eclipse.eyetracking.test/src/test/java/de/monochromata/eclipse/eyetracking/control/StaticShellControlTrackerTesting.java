package de.monochromata.eclipse.eyetracking.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.monochromata.eclipse.eyetracking.TestableEyeTracker;
import de.monochromata.eyetracking.Point;

public interface StaticShellControlTrackerTesting extends ShellTesting {

	default TestableEyeTracker createTestableEyeTracker() {
		final TestableEyeTracker testableEyeTracker = new TestableEyeTracker();
		// TODO: Add data to the testableEyeTracker
		return testableEyeTracker;
	}

	default Pair<StaticShellControlTracker, List<Pair<Point, Control>>> createControlTracker(final Shell shell,
			final TestableEyeTracker testableEyeTracker) {
		final StaticShellControlTracker controlTracker = new StaticShellControlTracker(testableEyeTracker, shell);
		final List<Pair<Point, Control>> controlEvents = new ArrayList<>();
		controlTracker.addListener((gaze, control) -> controlEvents.add(new ImmutablePair<>(gaze, control)));
		return new ImmutablePair<>(controlTracker, controlEvents);
	}

}
