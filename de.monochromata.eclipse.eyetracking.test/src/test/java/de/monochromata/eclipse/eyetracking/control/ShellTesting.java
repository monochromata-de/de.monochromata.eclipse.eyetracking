package de.monochromata.eclipse.eyetracking.control;

import static de.monochromata.eclipse.eyetracking.swt.SWTAccess.supplySync;
import static org.eclipse.swt.SWT.PUSH;

import java.util.function.BiConsumer;

import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public interface ShellTesting {

	default void withShell(final int x, final int y, final int width, final int height,
			final BiConsumer<Shell, Button> widgetConsumer) {
		final Shell shell = createTestShell(x, y, width, height);
		final Button button = createButton(shell, "myButton");
		showShell(x, y, width, height, shell);
		try {
			widgetConsumer.accept(shell, button);
		} finally {
			hideShell(shell);
		}
	}

	default Shell createTestShell(final int x, final int y, final int width, final int height) {
		return supplySync(Display.getDefault(), () -> {
			final Shell shell = new Shell();
			shell.setText("Test Shell");
			shell.setLayout(new RowLayout());
			return shell;
		});
	}

	default Button createButton(final Shell shell, final String text) {
		return supplySync(Display.getDefault(), () -> {
			final Button button = new Button(shell, PUSH);
			button.setText(text);
			return button;
		});
	}

	default void showShell(final int x, final int y, final int width, final int height, final Shell shell) {
		Display.getDefault().syncExec(() -> {
			shell.setBounds(x, y, width, height);
			shell.pack();
			shell.open();
		});
	}

	default void hideShell(final Shell shell) {
		Display.getDefault().syncExec(() -> {
			shell.close();
			shell.dispose();
		});
	}

}
