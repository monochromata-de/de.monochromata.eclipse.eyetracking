package de.monochromata.eclipse.eyetracking.control;

import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.getDisplayRelativeBounds;
import static de.monochromata.eclipse.eyetracking.swt.SWTAccess.supplySync;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.junit.Ignore;
import org.junit.Test;

import de.monochromata.eclipse.eyetracking.TestableEyeTracker;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;

public class StaticShellControlTrackerTest implements StaticShellControlTrackerTesting {

	// TODO: Figure out why this test does not work in Jenkins ... the shell
	// seems to be maximized?
	@Ignore
	@Test
	@SuppressWarnings("unchecked")
	public void assignControlToShell() {
		withShell(0, 0, 300, 200, (shell, button) -> {
			final TestableEyeTracker eyeTracker = createTestableEyeTracker();
			final Pair<StaticShellControlTracker, List<Pair<Point, Control>>> controlTrackerAndEvents = createControlTracker(
					shell, eyeTracker);
			final StaticShellControlTracker controlTracker = controlTrackerAndEvents.getLeft();
			final List<Pair<Point, Control>> events = controlTrackerAndEvents.getRight();

			// Send event to a location where there is no control
			eyeTracker.sendGazeEvent(500, 500);
			assertThat(events).hasSize(0);

			final Rectangle shellBounds = supplySync(shell, () -> shell.getBounds());

			// Send event to top left corner of the shell
			// The window border is at 0,0 but the window's contents are below
			// the border, i.e. at 0,14 (on GTK on Ubuntu).
			eyeTracker.sendGazeEvent(shellBounds.x, shellBounds.y);
			assertThat(events).containsExactly(new ImmutablePair<>(new PointImpl(shellBounds.x, shellBounds.y), shell));

			// Send event to shell, but not to button
			eyeTracker.sendGazeEvent(shellBounds.x + 2, shellBounds.y + 2);
			assertThat(events).containsExactly(new ImmutablePair<>(new PointImpl(shellBounds.x, shellBounds.y), shell),
					new ImmutablePair<>(new PointImpl(shellBounds.x + 2, shellBounds.y + 2), shell));

			// Send event to top left corner of the button in the shell
			final Rectangle displayRelativeButtonBounds = supplySync(shell, () -> getDisplayRelativeBounds(button));
			// Note that the display-relative bounds for the button include
			// the hidden menu of the shell which is why buttonY is not simply
			// shellBounds.y (which is already display-relative) +
			// button.getBounds().y .
			final int buttonX = displayRelativeButtonBounds.x;
			final int buttonY = displayRelativeButtonBounds.y;
			eyeTracker.sendGazeEvent(buttonX, buttonY);
			assertThat(events).containsExactly(new ImmutablePair<>(new PointImpl(shellBounds.x, shellBounds.y), shell),
					new ImmutablePair<>(new PointImpl(shellBounds.x + 2, shellBounds.y + 2), shell),
					new ImmutablePair<>(new PointImpl(buttonX, buttonY), button));

			assertThat(controlTracker).isNotNull();
		});
	}

}
