package de.monochromata.eclipse.eyetracking.rfl.strategy;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.internal.ide.filesystem.FileSystemStructureProvider;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.intro.IIntroPart;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;
import org.junit.Before;

/**
 * Abstract base class for tests of sub-classes of
 * {@link MouseEventCollectionStrategy}.
 */
public class AbstractManualContextCollectionStrategyTest {

	public static final String TEST_RESOURCES_PROJECT_NAME = "de.monochromata.eclipse.eyetracking.test.resources";
	public static final String TEST_RESOURCES_PROJECT_PATH_STRING = "/" + TEST_RESOURCES_PROJECT_NAME;
	public static final String TEST_RESOURCES_PROJECT_RELATIVE_PATH_STRING = ".." + TEST_RESOURCES_PROJECT_PATH_STRING;

	// TODO: Use the new solution from the Git repo of SWTBot
	// @Rule
	// public CaptureScreenshotOnErrorOrFailure screenshot = new
	// CaptureScreenshotOnErrorOrFailure();

	private final SWTWorkbenchBot bot = new SWTWorkbenchBot();

	protected SWTWorkbenchBot getBot() {
		return bot;
	}

	protected void closeIntroIfExists() {
		final IIntroManager introManager = PlatformUI.getWorkbench().getIntroManager();
		final IIntroPart intro = introManager.getIntro();
		if (intro != null) {
			performInUIThread(() -> {
				final boolean introClosed = introManager.closeIntro(intro);
				assertThat(introClosed).describedAs("Intro is closed").isEqualTo(true);
			});
		}
	}

	protected void showJavaPerspective() throws InterruptedException {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		assertThat(workbench.isStarting()).isFalse();
		performInUIThread(() -> {
			try {
				final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
				assertThat(activeWorkbenchWindow).isNotNull();
				final String perspectiveId = JavaUI.ID_PERSPECTIVE;
				workbench.showPerspective(perspectiveId, activeWorkbenchWindow);
			} catch (final Exception e) {
				throw new RuntimeException("Unexpected exception", e);
			}
		});
	}

	protected void setEditorAreaVisible() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		performInUIThread(() -> {
			final IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
			final IWorkbenchPage page = activeWorkbenchWindow.getActivePage();
			page.setEditorAreaVisible(true);
		});
	}

	protected void makeSureTestResourcesProjectExists()
			throws CoreException, InvocationTargetException, InterruptedException, IOException {
		final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		final IPath testResourcesPath = new Path(TEST_RESOURCES_PROJECT_PATH_STRING);
		final IProject testResourcesProject = workspaceRoot.getProject(TEST_RESOURCES_PROJECT_NAME);
		if (!testResourcesProject.exists()) {
			importTestResourcesProject(testResourcesPath, testResourcesProject);
		}
	}

	protected void importTestResourcesProject(final IPath testResourcesPath, final IProject testResourcesProject)
			throws InvocationTargetException, InterruptedException {
		System.err.println("Importing " + testResourcesProject);

		final File sourceDirectory = new File(TEST_RESOURCES_PROJECT_RELATIVE_PATH_STRING);
		assertThat(sourceDirectory)
				.describedAs("Directory for test resource project: " + TEST_RESOURCES_PROJECT_RELATIVE_PATH_STRING + "="
						+ sourceDirectory.getAbsolutePath()
						+ " exists. Make sure to run the tests in a sibling directory of the test resources project")
				.exists();

		final FileSystemStructureProvider fileSystemStructureProvider = new FileSystemStructureProvider();
		final IOverwriteQuery overwriteQuery = new IOverwriteQuery() {

			@Override
			public String queryOverwrite(final String pathString) {
				return ALL;
			}
		};
		final ImportOperation operation = new ImportOperation(testResourcesPath, sourceDirectory,
				fileSystemStructureProvider, overwriteQuery, Arrays.asList(new File[] { sourceDirectory }));
		performInUIThread(() -> {
			operation.setContext(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		});
		operation.setCreateContainerStructure(false);
		operation.run(null);
		assertThat(operation.getStatus().isOK()).isTrue();
	}

	protected void performInUIThread(final Runnable runnable) {
		PlatformUI.getWorkbench().getDisplay().syncExec(runnable);
	}

	@Before
	public void setUp() throws InterruptedException, InvocationTargetException, CoreException, IOException {
		closeIntroIfExists();
		showJavaPerspective();
		setEditorAreaVisible();
		makeSureTestResourcesProjectExists();
	}

}
