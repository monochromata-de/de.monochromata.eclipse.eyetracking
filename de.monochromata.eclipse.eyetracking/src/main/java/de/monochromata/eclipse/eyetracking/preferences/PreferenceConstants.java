package de.monochromata.eclipse.eyetracking.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String ENABLE_EYETRACKING = "enableEyetracking";

}
