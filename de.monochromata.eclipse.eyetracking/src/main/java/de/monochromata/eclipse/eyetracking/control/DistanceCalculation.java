package de.monochromata.eclipse.eyetracking.control;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.eyetracking.swt.SWTAccess;
import de.monochromata.eyetracking.Point;

/**
 * The methods of this interface need to be invoked from the SWT user-interface
 * thread.
 * 
 * @see SWTAccess
 * @see Display#syncExec(Runnable)
 * @see Display#asyncExec(Runnable)
 */
public interface DistanceCalculation {

	static double distanceToRectangle(final Rectangle bounds, final Point point) {
		return distanceToRectangle(bounds, point.getX(), point.getY());
	}

	static double distanceToRectangle(final Rectangle bounds, final int x, final int y) {
		double xDistance2 = 0.0d;
		if (!(bounds.x <= x && x <= bounds.x + bounds.width)) {
			// not inside rectangle wrt. x axis
			final double closestEdgeX = Math.max(Math.min(x, bounds.x + bounds.width), bounds.x);
			xDistance2 = Math.pow(x - closestEdgeX, 2);
		}

		double yDistance2 = 0.0d;
		if (!(bounds.y <= y && y <= bounds.y + bounds.height)) {
			// not inside rectangle wrt. y axis
			final double closestEdgeY = Math.max(Math.min(y, bounds.y + bounds.height), bounds.y);
			yDistance2 = Math.pow(y - closestEdgeY, 2);
		}

		final double distance = Math.sqrt(xDistance2 + yDistance2);
		return distance;
	}

	static boolean hitsBounds(final Point gaze, final Rectangle displayRelativeBounds) {
		return displayRelativeBounds.contains(gaze.getX(), gaze.getY());
	}

	public static Rectangle getDisplayRelativeBounds(final Control control) {
		final Rectangle bounds = control.getBounds();
		final Composite parent = control.getParent();
		if (parent == null) {
			// If parent is null, bounds are display-relative already.
			return bounds;
		}
		return parent.getDisplay().map(parent, null /* i.e. display */, bounds);
	}

}
