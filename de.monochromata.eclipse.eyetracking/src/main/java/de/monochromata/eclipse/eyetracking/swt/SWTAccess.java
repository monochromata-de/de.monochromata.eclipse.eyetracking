package de.monochromata.eclipse.eyetracking.swt;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

public interface SWTAccess {

	static <T> T supplySync(final Control control, final Supplier<T> supplier) {
		return supplySync(control.getDisplay(), supplier);
	}

	static <T> T supplySync(final Display display, final Supplier<T> supplier) {
		final AtomicReference<T> result = new AtomicReference<>();
		display.syncExec(() -> result.set(supplier.get()));
		return result.get();
	}

}
