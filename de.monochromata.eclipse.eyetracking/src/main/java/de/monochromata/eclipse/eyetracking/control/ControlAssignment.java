package de.monochromata.eclipse.eyetracking.control;

import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.distanceToRectangle;
import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.getDisplayRelativeBounds;
import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.hitsBounds;
import static java.util.Arrays.asList;

import java.util.LinkedList;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.eyetracking.swt.SWTAccess;
import de.monochromata.eyetracking.Point;

/**
 * The methods of this interface need to be invoked from the SWT user-interface
 * thread.
 * 
 * @see SWTAccess
 * @see Display#syncExec(Runnable)
 * @see Display#asyncExec(Runnable)
 */
public interface ControlAssignment {

	static Pair<Control, Rectangle> findControlInTree(final Point gaze, final Control control) {
		// TODO: Have a look at the old ControlAssignmentFilter for further
		// testing and algorithmic details
		final LinkedList<Control> nextControls = new LinkedList<>();
		nextControls.add(control);
		double narrowestDistance = Double.MAX_VALUE;
		Control narrowestControl = null;
		Rectangle narrowestControlDisplayRelativeBounds = null;
		while (!nextControls.isEmpty()) {
			final Control nextControl = nextControls.removeFirst();
			if (!nextControl.isDisposed() && nextControl.isVisible()) {
				final Rectangle displayRelativeBounds = getDisplayRelativeBounds(nextControl);
				if (hitsBounds(gaze, displayRelativeBounds)) {
					final double distance = distanceToRectangle(displayRelativeBounds, gaze);
					if (distance < narrowestDistance) {
						narrowestDistance = distance;
						narrowestControl = nextControl;
						narrowestControlDisplayRelativeBounds = displayRelativeBounds;
					} else if (distance == narrowestDistance) {
						// If children are equidistant, choose the control
						// farther to the top or to the right. And if they are
						// equal, overwrite narrowestControl with nextControl.
						// TODO: Why is not condition not OR'ed?
						if (displayRelativeBounds.y <= narrowestControlDisplayRelativeBounds.y
								|| displayRelativeBounds.x >= narrowestControlDisplayRelativeBounds.x) {
							// would be redundant due to the if condition:
							// narrowestDistance = distance;
							narrowestControl = nextControl;
							narrowestControlDisplayRelativeBounds = displayRelativeBounds;
						}
					}
					if (nextControl instanceof Composite) {
						nextControls.addAll(asList(((Composite) nextControl).getChildren()));
					}
				}
			}
		}
		return new ImmutablePair<>(narrowestControl, narrowestControlDisplayRelativeBounds);
	}

}
