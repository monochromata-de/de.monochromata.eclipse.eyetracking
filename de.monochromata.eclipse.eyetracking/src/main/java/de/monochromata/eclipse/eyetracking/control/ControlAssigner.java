package de.monochromata.eclipse.eyetracking.control;

import static de.monochromata.eclipse.eyetracking.EyeTracking.PLUGIN_ID;
import static de.monochromata.eclipse.eyetracking.control.ControlAssignment.findControlInTree;
import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.getDisplayRelativeBounds;
import static de.monochromata.eclipse.eyetracking.control.DistanceCalculation.hitsBounds;
import static java.lang.System.identityHashCode;
import static org.eclipse.core.runtime.IStatus.WARNING;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.statushandlers.StatusManager;

import de.monochromata.eclipse.eyetracking.swt.SWTAccess;
import de.monochromata.eyetracking.Point;

/**
 * The methods of this class need to be invoked from the SWT user-interface
 * thread.
 * 
 * @see SWTAccess
 * @see Display#syncExec(Runnable)
 * @see Display#asyncExec(Runnable)
 */
public class ControlAssigner {

	private final Shell shell;
	private Control lastControl;
	private Rectangle lastControlDisplayRelativeBounds;

	public ControlAssigner(final Shell shell) {
		this.shell = shell;
	}

	protected Control findControl(final Point gaze) {
		// TODO: Write tests for all sub-conditions
		if (lastControl != null && !lastControl.isDisposed() && lastControl.isVisible()
				&& (!(lastControl instanceof Composite) || ((Composite) lastControl).getChildren().length == 0)
				&& lastControlDisplayRelativeBounds.equals(getDisplayRelativeBounds(lastControl))
				&& hitsBounds(gaze, lastControlDisplayRelativeBounds)) {
			return lastControl;
		} else if (shell.isDisposed()) {
			StatusManager.getManager()
					.handle(new Status(WARNING, PLUGIN_ID, "Shell is disposed: " + identityHashCode(shell)));
			return null;
		}
		final Pair<Control, Rectangle> result = findControlInTree(gaze, shell);
		if (result != null) {
			lastControl = result.getLeft();
			lastControlDisplayRelativeBounds = result.getRight();
		}
		return lastControl;
	}

}
