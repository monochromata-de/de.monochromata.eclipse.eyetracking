package de.monochromata.eclipse.eyetracking.rfl.strategy;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.time.Clock;
import de.monochromata.eyetracking.time.SystemClock;

/**
 * When a word
 */
public class DoubleClickSelectionStrategy extends AbstractMouseEventCollectionStrategy {

	private final Listener mouseListener;
	private final Listener selectionListener;

	/**
	 * Create a new instance that uses a {@link SystemClock} to determine the time
	 * at which a {@link Point} is created.
	 */
	public DoubleClickSelectionStrategy() {
		this(new SystemClock());
	}

	/**
	 * Create a new instance that uses the given clock to determine the time at
	 * which a {@link Point} is created.
	 * 
	 * @param clock the clock to use
	 */
	public DoubleClickSelectionStrategy(final Clock clock) {
		this.mouseListener = event -> {
			System.err.println(getEventType(event) + " detect at " + event.x + "," + event.y + " bounds="
					+ event.getBounds() + " widget=" + event.widget);
		};
		this.selectionListener = event -> {
			System.err.print("text selection detect at " + event.x + "," + event.y + " bounds=" + event.getBounds()
					+ " widget=" + event.widget);
			// TODO: Does this also work when a tree item is selected via click
			// / mouse movement?
			if (event.widget instanceof StyledText) {
				final StyledText styledText = (StyledText) event.widget;
				System.err.print(styledText.getBlockSelection() + " " + styledText.getBlockSelectionBounds() + " "
						+ styledText.getSelection() + " " + styledText.getSelectionCount() + " "
						+ styledText.getSelectionRange() + " " + asList(styledText.getSelectionRanges()) + " "
						+ styledText.getSelectionText());
			}
			System.err.println();
			// TODO: Auch die details abfragen? Nur bei SWT.MENU_MOUSE feuern
			notifyListeners(event);
		};
	}

	private static String getEventType(final Event event) {
		switch (event.type) {
		case SWT.MouseDown:
			return "MouseDown";
		case SWT.MouseDoubleClick:
			return "MouseDoubleClick";
		case SWT.MouseUp:
			return "MouseUp";
		default:
			throw new IllegalArgumentException("Unknown event type: " + event.type);
		}
	}

	private String asList(final int[] array) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			sb.append((i == 0 ? "" : ", ") + array[i]);
		}
		return sb.toString();
	}

	@Override
	protected void installFiltersFor(final Display display) {
		System.err.println("Add " + this + " to " + display);
		display.addFilter(SWT.MouseDown, mouseListener);
		display.addFilter(SWT.MouseUp, mouseListener);
		display.addFilter(SWT.MouseDoubleClick, mouseListener);
		display.addFilter(SWT.Selection, selectionListener);
	}

	@Override
	protected void uninstallFiltersFor(final Display display) {
		display.removeFilter(SWT.Selection, selectionListener);
		display.removeFilter(SWT.MouseDoubleClick, mouseListener);
		display.removeFilter(SWT.MouseUp, mouseListener);
		display.removeFilter(SWT.MouseDown, mouseListener);
	}

}
