package de.monochromata.eclipse.eyetracking.rfl;

import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.swt.widgets.Display;

import de.monochromata.eclipse.eyetracking.rfl.strategy.MouseEventCollectionStrategy;
import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.Point;

public class MouseEventTracker extends AbstractTracker<Consumer<Point>> implements AutoCloseable {

	private final List<Display> displays;
	private final List<MouseEventCollectionStrategy> strategies = new ArrayList<>();
	private final Consumer<Point> forwardingEventListener = point -> notifyListeners(
			listener -> listener.accept(point));

	public MouseEventTracker(final Display display) {
		this(singletonList(display));
	}

	public MouseEventTracker(final List<Display> displays) {
		this.displays = displays;
	}

	public synchronized void addStrategy(final MouseEventCollectionStrategy strategy) {
		strategy.install(displays);
		strategies.add(strategy);
		strategy.addListener(forwardingEventListener);
	}

	@Override
	public synchronized void close() throws Exception {
		strategies.stream().forEach(MouseEventCollectionStrategy::uninstall);
		strategies.stream().forEach(strategy -> strategy.removeListener(forwardingEventListener));
	}

}
