package de.monochromata.eclipse.eyetracking.rfl.strategy;

import java.util.function.Consumer;

import de.monochromata.event.Tracker;
import de.monochromata.eyetracking.Point;

public interface MouseEventCollectionStrategy extends Installable, Tracker<Consumer<Point>> {

}
