package de.monochromata.eclipse.eyetracking.rfl.strategy;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.time.Clock;
import de.monochromata.eyetracking.time.SystemClock;

/**
 * When a context-menu is opened, this strategy collects a {@link Point} for
 * this event.
 */
public class MenuDetectStrategy extends AbstractMouseEventCollectionStrategy {

	/**
	 * This listener is triggered, when a new context menu is opened
	 * 
	 * <p>
	 * TODO: Move to separate class ContextMenuStrategy, needs to consult detail
	 * SWT.MENU_MOUSE ... it would also be possible to correlate with
	 * SWT.MENU_KEYBOARD, but then with the text cursor of the current editor (might
	 * need to track activate for Editors)
	 * 
	 * <p>
	 * TODO: Auch event.doit beachten ...
	 * 
	 * <p>
	 * TODO: Wobei bei SWT.MENU_KEYBOARD auch noch die Informationen verwendet
	 * werden könnten, dass ins Rectangle geschaut worden sein muss
	 * 
	 * <p>
	 * TODO: Wobei, wenn z.B. Buttons wie OK etc. per Keyboard aktiviert werden,
	 * darf diese Information nicht als RFL verwertet werden, weil dann nicht klar
	 * ist, ob die nicht routiniert selektiert werden, weil sowieso schon bekannt
	 * ist, dass dieser Button kommt. Das könnte unabhängig von OK, CANCEL etc. auch
	 * für oft verwendete Buttons gelten.
	 */
	private final Listener menuDetectListener;

	/**
	 * Create a new instance that uses a {@link SystemClock} to determine the time
	 * at which a {@link Point} is created.
	 */
	public MenuDetectStrategy() {
		this(new SystemClock());
	}

	/**
	 * Create a new instance that uses the given clock to determine the time at
	 * which a {@link Point} is created.
	 * 
	 * @param clock the clock to use
	 */
	public MenuDetectStrategy(final Clock clock) {
		this.menuDetectListener = event -> {
			System.err.println("menu detect at " + event.x + "," + event.y + " bounds=" + event.getBounds() + " widget="
					+ event.widget);
			// TODO: Auch die details abfragen? Nur bei SWT.MENU_MOUSE feuern
			notifyListeners(event);
		};
	}

	@Override
	protected void installFiltersFor(final Display display) {
		// TODO: Use SWT.MENU_MOUSE and SWT.MENU_KEYBOARD
		display.addFilter(SWT.MenuDetect, menuDetectListener);
	}

	@Override
	protected void uninstallFiltersFor(final Display display) {
		display.removeFilter(SWT.MenuDetect, menuDetectListener);
	}

}
