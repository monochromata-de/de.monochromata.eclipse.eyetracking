/**
 * Assigns controls to gaze events.
 */
package de.monochromata.eclipse.eyetracking.control;