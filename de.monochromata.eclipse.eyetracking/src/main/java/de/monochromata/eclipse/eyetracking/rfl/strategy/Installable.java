package de.monochromata.eclipse.eyetracking.rfl.strategy;

import java.util.Collection;

import org.eclipse.swt.widgets.Display;

public interface Installable {

	void install(Collection<Display> displays);

	void uninstall();

}
