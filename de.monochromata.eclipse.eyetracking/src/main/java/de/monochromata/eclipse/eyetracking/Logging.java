package de.monochromata.eclipse.eyetracking;

import static org.eclipse.core.runtime.IStatus.ERROR;
import static org.eclipse.ui.statushandlers.StatusManager.LOG;
import static org.eclipse.ui.statushandlers.StatusManager.SHOW;

import java.util.function.Consumer;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

public interface Logging {

	static void logError(final RunnableWithException runnable) {
		handleError(runnable, ex -> logError(ex.getMessage(), ex));
	}

	static void showError(final RunnableWithException runnable) {
		handleError(runnable, ex -> showError(ex.getMessage(), ex));
	}

	static void logAndShowError(final RunnableWithException runnable) {
		handleError(runnable, ex -> {
			logError(ex.getMessage(), ex);
			showError(ex.getMessage(), ex);
		});
	}

	static void handleError(final RunnableWithException runnable, final Consumer<Exception> exceptionHandler) {
		try {
			runnable.run();
		} catch (final Exception ex) {
			exceptionHandler.accept(ex);
		}

	}

	@FunctionalInterface
	interface RunnableWithException {
		public void run() throws Exception;
	}

	static void logError(final String message, final Exception exception) {
		handleError(message, exception, LOG);
	}

	static void showError(final String message, final Exception exception) {
		handleError(message, exception, SHOW);
	}

	static void handleError(final String message, final Exception exception, final int style) {
		StatusManager.getManager().handle(new Status(ERROR, EyeTracking.PLUGIN_ID, message, exception), style);
	}

}
