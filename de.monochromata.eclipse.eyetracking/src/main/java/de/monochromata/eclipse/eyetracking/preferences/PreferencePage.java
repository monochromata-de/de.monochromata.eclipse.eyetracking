package de.monochromata.eclipse.eyetracking.preferences;

import static de.monochromata.eclipse.eyetracking.preferences.PreferenceConstants.ENABLE_EYETRACKING;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.monochromata.eclipse.eyetracking.EyeTracking;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public PreferencePage() {
		super(GRID);
		setPreferenceStore(EyeTracking.getDefault().getPreferenceStore());
		setDescription("Configuration of eye tracking in Eclipse");
	}

	@Override
	public void createFieldEditors() {
		addField(new BooleanFieldEditor(ENABLE_EYETRACKING, "&Whether or not to track eye movements",
				getFieldEditorParent()));
	}

	@Override
	public void init(final IWorkbench workbench) {
	}

}