package de.monochromata.eclipse.eyetracking;

import static de.monochromata.eclipse.eyetracking.EyeTracking.EYE_TRACKERS_EXTENSION_POINT;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

import de.monochromata.eyetracking.ControllableEyeTracker;

public class Util {

	private static List<ControllableEyeTracker> availableEyeTrackers;

	public static List<ControllableEyeTracker> getAvailableEyeTrackers() {
		return availableEyeTrackers = getPluginObjects(availableEyeTrackers, EYE_TRACKERS_EXTENSION_POINT);
	}

	public static ControllableEyeTracker getEyeTracker() {
		if (availableEyeTrackers == null) {
			getAvailableEyeTrackers();
		}
		// TODO: Provide a dropdown in the preferences;
		if (!availableEyeTrackers.isEmpty()) {
			final ControllableEyeTracker firstTracker = availableEyeTrackers.get(0);
			firstTracker.setSelected(true);
			return firstTracker;
		}
		return null;
	}

	public static <T> List<T> getPluginObjects(final List<T> list, final String extensionPointId) {
		if (list == null) {
			return loadPluginObjects(extensionPointId);
		}
		return list;
	}

	private static <T> List<T> loadPluginObjects(final String extensionPointId) {
		final List<T> adapters = new ArrayList<T>();
		final IExtensionRegistry registry = Platform.getExtensionRegistry();
		final IExtensionPoint extensionPoint = registry.getExtensionPoint(extensionPointId);
		final IExtension[] extensions = extensionPoint.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			loadPluginObject(adapters, extensions[i]);
		}
		return adapters;
	}

	private static <T> void loadPluginObject(final List<T> adapters, final IExtension extension) {
		final IConfigurationElement[] elements = extension.getConfigurationElements();
		for (int j = 0; j < elements.length; j++) {
			try {
				// Provoke a ClassCastException because we cannot access the
				// type T at runtime
				// to ensure that the returned object is an instance of T
				@SuppressWarnings("unchecked")
				final T extInstance = (T) elements[j].createExecutableExtension("class");
				adapters.add(extInstance);
			} catch (final Exception e) {
				logAndShowException("Error while loading plugin objects", e);
			}
		}
	}

	public static void logInfo(final String message) {
		StatusManager.getManager().handle(new Status(IStatus.INFO, EyeTracking.PLUGIN_ID, message), StatusManager.LOG);
	}

	public static void logAndShowInfo(final String message) {
		StatusManager.getManager().handle(new Status(IStatus.INFO, EyeTracking.PLUGIN_ID, message),
				StatusManager.LOG | StatusManager.SHOW);
	}

	public static void logAndShowBlockingInfo(final String message) {
		StatusManager.getManager().handle(new Status(IStatus.INFO, EyeTracking.PLUGIN_ID, message),
				StatusManager.LOG | StatusManager.SHOW | StatusManager.BLOCK);
	}

	public static void logError(final String message) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, EyeTracking.PLUGIN_ID, message), StatusManager.LOG);
	}

	public static void logAndShowError(final String message) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, EyeTracking.PLUGIN_ID, message),
				StatusManager.LOG | StatusManager.SHOW);
	}

	public static void logException(final String message, final Exception e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, EyeTracking.PLUGIN_ID, message, e),
				StatusManager.LOG);
	}

	public static void logAndShowException(final String message, final Exception e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, EyeTracking.PLUGIN_ID, message, e),
				StatusManager.LOG | StatusManager.SHOW);
	}
}
