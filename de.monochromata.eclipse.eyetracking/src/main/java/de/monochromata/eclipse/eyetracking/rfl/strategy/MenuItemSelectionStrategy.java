package de.monochromata.eclipse.eyetracking.rfl.strategy;

import static de.monochromata.eclipse.eyetracking.rfl.strategy.StrategyUtils.getBounds;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

/**
 * A {@link MouseEventCollectionStrategy} that emits a ManualContext whenever
 * a MenuItem is selected (i.e. clicked).
 */
public class MenuItemSelectionStrategy extends AbstractMouseEventCollectionStrategy {

	/**
	 * Is triggered when a menu item is selected
	 * 
	 * <p>
	 * TODO: Rename class to include "Selection"
	 * 
	 * <p>
	 * TODO: Handle menu items that contain a tooltip differently because
	 * fixations might follow their tooltip?
	 * 
	 * <p>
	 * TODO: Might not yield meaningful results for the shell menuBar - at least
	 * on Ubuntu / GTK2 coordinates are 0 for the menu items contained therein.
	 * 
	 * <p>
	 * TODO: Might need to use detail | SWT.MENU_MOUSE because selection might
	 * also be triggered by <ENTER> ... wobei dann immernoch die Region als
	 * grobkörnigere Information verwendet werden könnte
	 */
	private final Listener selectionListener = event -> {
	};

	@Override
	protected void installFiltersFor(final Display display) {
		// TODO: Use SWT.MENU_MOUSE and SWT.MENU_KEYBOARD
		display.addFilter(SWT.Selection, selectionListener);
	}

	@Override
	protected void uninstallFiltersFor(final Display display) {
		display.removeFilter(SWT.Selection, selectionListener);
	}

	private class ListenerImpl implements Listener {

		private final String eventName;

		public ListenerImpl(final String eventName) {
			this.eventName = eventName;
		}

		@Override
		public void handleEvent(final Event event) {
			// TODO: Also log/filter the mouse button
			// TODO: Need to map widget-relative coordinates to absolute ones -
			// see
			// org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl.absoluteLocation()

			System.err.println(eventName + " at " + event.x + "," + event.y + " bounds=" + event.getBounds() + " item="
					+ event.item + " widget=" + event.widget);
			if (event.item != null && event.item instanceof MenuItem && event.widget != null
					&& event.widget instanceof Menu) {
				final Menu menu = (Menu) event.widget;
				final MenuItem menuItem = (MenuItem) event.item;
				System.err.println("Bounds of menu " + event.widget + "=" + getBounds(menu));
				System.err.println("Bounds of menu item " + event.item + "=" + getBounds(menuItem));
			} else if (event.widget != null && event.widget instanceof MenuItem) {
				System.err.println(
						"Bounds of menu item widget " + event.widget + "=" + getBounds((MenuItem) event.widget));
			} else if (event.widget != null && event.widget instanceof Menu) {
				final Menu menu = (Menu) event.widget;
				System.err.println("Bounds of menu " + getBounds(menu) + " parentItemBounds="
						+ (menu.getParentItem() != null ? getBounds(menu.getParentItem()) : null) + " parentMenuBounds="
						+ (menu.getParentMenu() != null ? getBounds(menu.getParentMenu()) : null));
			}
		}

	}

}
