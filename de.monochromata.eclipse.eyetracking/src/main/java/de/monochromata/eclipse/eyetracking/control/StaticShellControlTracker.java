package de.monochromata.eclipse.eyetracking.control;

import static de.monochromata.eclipse.eyetracking.swt.SWTAccess.supplySync;
import static java.util.Objects.requireNonNull;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.EyeTracker;
import de.monochromata.eyetracking.Point;

/**
 * Assigns controls from a given {@link Shell} to gaze events received from a
 * given {@link EyeTracker}.
 * <p>
 * TODO: Add a sub-class that is able to detect controls in multiple shells and
 * retains the active set of shells as new shells are
 * opened/closed/activated/deactivated.
 */
public class StaticShellControlTracker extends AbstractTracker<BiConsumer<Point, Control>> implements ControlTracker {

	private final Consumer<Point> gazeListener = this::assignControl;
	private final EyeTracker eyeTracker;
	private final Shell shell;
	private final ControlAssigner controlAssigner;

	public StaticShellControlTracker(final EyeTracker eyeTracker, final Shell shell) {
		this.eyeTracker = requireNonNull(eyeTracker);
		this.shell = requireNonNull(shell);
		this.controlAssigner = new ControlAssigner(shell);
		initialize();
	}

	protected void initialize() {
		eyeTracker.addListener(gazeListener);
	}

	protected void assignControl(final Point gaze) {
		final Control control = supplySync(shell, () -> controlAssigner.findControl(gaze));
		if (control != null) {
			notifyListeners(gaze, control);
		}
	}

	protected void notifyListeners(final Point gaze, final Control control) {
		notifyListeners(listener -> listener.accept(gaze, control));
	}

	@Override
	public void close() throws Exception {
		eyeTracker.removeListener(gazeListener);
	}

}
