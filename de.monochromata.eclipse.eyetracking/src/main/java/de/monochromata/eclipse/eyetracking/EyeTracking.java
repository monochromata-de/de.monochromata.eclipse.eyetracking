package de.monochromata.eclipse.eyetracking;

import static de.monochromata.eclipse.eyetracking.preferences.PreferenceConstants.ENABLE_EYETRACKING;
import static java.lang.Boolean.parseBoolean;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.monochromata.eclipse.eyetracking.rfl.strategy.DoubleClickSelectionStrategy;
import de.monochromata.eyetracking.ControllableEyeTracker;
import de.monochromata.eyetracking.DelegatingEyeTracker;
import de.monochromata.eyetracking.EyeTracker;

/**
 * The activator class controls the plug-in life cycle.
 */
public class EyeTracking extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.monochromata.eclipse.eyetracking"; //$NON-NLS-1$
	public static final String EYE_TRACKERS_EXTENSION_POINT = PLUGIN_ID + ".eyeTrackers";

	// The shared instance
	private static EyeTracking plugin;

	private State state = new EyeTrackingDisabled();
	private final DelegatingEyeTracker delegatingEyeTracker = new DelegatingEyeTracker();

	private final IPropertyChangeListener propertyChangeListener = event -> {
		System.err.println("Preference property changed: " + event.getProperty() + " " + event.getOldValue() + " -> "
				+ event.getNewValue());
		if (event.getProperty().equals(ENABLE_EYETRACKING)) {
			final Object newValue = event.getNewValue();
			enableEyeTracking(newValue instanceof String ? parseBoolean((String) newValue) : (Boolean) newValue);
		}
	};

	private DoubleClickSelectionStrategy doubleClickStrategy;

	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		getPreferenceStore().addPropertyChangeListener(propertyChangeListener);
		enableEyeTracking(getPreferenceStore().getBoolean(ENABLE_EYETRACKING));

		/*
		 * // TODO: Remove - only for development final Display display =
		 * PlatformUI.getWorkbench().getDisplay(); doubleClickStrategy = new
		 * DoubleClickSelectionStrategy();
		 * doubleClickStrategy.install(singletonList(display));
		 */
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		getPreferenceStore().removePropertyChangeListener(propertyChangeListener);
		plugin = null;
		super.stop(context);
		enableEyeTracking(false);
	}

	private void enableEyeTracking(final boolean enable) {
		state.enableEyeTracking(enable);
	}

	public static EyeTracking getDefault() {
		return plugin;
	}

	/**
	 * @return a {@link DelegatingEyeTracker} that delegates the events of the
	 *         currently selected eye tracker and is available regardless of whether
	 *         there is an underlying eye tracker or whether the underlying eye
	 *         tracker changes.
	 */
	public EyeTracker getEyeTracker() {
		return delegatingEyeTracker;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in relative
	 * path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	private interface State {
		void enableEyeTracking(boolean enable);

		EyeTracker getEyeTracker();
	}

	private class EyeTrackingDisabled implements State {
		@Override
		public synchronized void enableEyeTracking(final boolean enable) {
			if (enable) {
				state = new EyeTrackingStarting();
			}
		}

		@Override
		public EyeTracker getEyeTracker() {
			return null;
		}
	}

	private class EyeTrackingStarting implements State {

		public EyeTrackingStarting() {
			// Start the eye-tracker asynchronously as obtaining the sarxos
			// webcam for the OpenIMAJ driver might hang in the Eclipse main
			// thread on Mac OS. See the following issues:
			// https://github.com/sarxos/webcam-capture/issues/357
			// https://github.com/sarxos/webcam-capture/issues/170
			final Job startupJob = Job.create("Starting eye tracker", monitor -> {
				final ControllableEyeTracker tracker = Util.getEyeTracker();
				tracker.setTracking(true);
				delegatingEyeTracker.setDelegate(tracker);
				state = new EyeTrackingEnabled(tracker);
				return Status.OK_STATUS;
			});
			startupJob.setPriority(Job.SHORT);
			startupJob.schedule();
		}

		/**
		 * Ignores any calls except the one that is progressing right now.
		 */
		@Override
		public void enableEyeTracking(final boolean enable) {
		}

		@Override
		public EyeTracker getEyeTracker() {
			return null;
		}

	}

	private class EyeTrackingEnabled implements State {

		private final ControllableEyeTracker tracker;

		public EyeTrackingEnabled(final ControllableEyeTracker tracker) {
			this.tracker = tracker;
		}

		@Override
		public void enableEyeTracking(final boolean enable) {
			if (!enable) {
				delegatingEyeTracker.setDelegate(null);
				tracker.setTracking(false);
				state = new EyeTrackingDisabled();
			}
		}

		@Override
		public EyeTracker getEyeTracker() {
			return tracker;
		}

	}

	/*
	 * private class EyeTrackingEnabled implements State {
	 * 
	 * private final ContextCollection contextCollection; private TheEyeTribeSource
	 * eyeSource; private ManualContextSource manualSource;
	 * 
	 * private EyeTrackingEnabled() throws Exception { // Cf. //
	 * https://wiki.eclipse.org/FAQ_Where_do_plug-ins_store_their_state%3F final
	 * Location userLocation = Platform.getUserLocation();
	 * requireNonNull(userLocation, "No user location available"); final URI
	 * dataArea = userLocation.getDataArea(PLUGIN_ID).toURI(); contextCollection =
	 * new ContextCollection(dataArea, getEyeContextSource(),
	 * getManualContextSource()); }
	 * 
	 * private TheEyeTribeSource getEyeContextSource() { if (eyeSource == null) {
	 * eyeSource = new TheEyeTribeSource(Locale.getDefault(),
	 * TimeZone.getDefault()); } return eyeSource; }
	 * 
	 * private ManualContextSource getManualContextSource() { if (manualSource ==
	 * null) { manualSource = new
	 * ManualContextSource(PlatformUI.getWorkbench().getDisplay());
	 * manualSource.addStrategy(new MenuDetectStrategy());
	 * manualSource.addStrategy(new DoubleClickSelectionStrategy()); } return
	 * manualSource; }
	 * 
	 * @Override public synchronized void enableEyeTracking(final boolean enable) {
	 * if (!enable) { getEyeContextSource().close(); logError(() ->
	 * getManualContextSource().close()); showError(contextCollection::close); state
	 * = new EyeTrackingDisabled(); eyeSource = null; manualSource = null; } }
	 * 
	 * }
	 */
}
