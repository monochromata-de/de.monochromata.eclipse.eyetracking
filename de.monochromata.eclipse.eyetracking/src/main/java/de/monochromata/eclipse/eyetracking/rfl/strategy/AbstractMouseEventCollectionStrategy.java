package de.monochromata.eclipse.eyetracking.rfl.strategy;

import java.util.Collection;
import java.util.WeakHashMap;
import java.util.function.Consumer;

import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;

/**
 * An abstract base class for implementations of
 * {@link MouseEventCollectionStrategy}.
 * 
 * <p>
 * This class is not thread-safe.
 */
public abstract class AbstractMouseEventCollectionStrategy extends AbstractTracker<Consumer<Point>>
		implements MouseEventCollectionStrategy {

	protected State state = new Uninstalled();
	protected WeakHashMap<Display, ?> displays;

	protected void notifyListeners(final Event event) {
		final PointImpl point = new PointImpl(event.x, event.y);
		notifyListeners(listener -> listener.accept(point));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param displays
	 *            {@inheritDoc}
	 * @throws IllegalStateException
	 *             {@inheritDoc}
	 * @throws SWTException
	 *             if this method is not invoked from the event-dispatching
	 *             thread
	 * @see #uninstall()
	 */
	@Override
	public void install(final Collection<Display> displays) {
		state.install(displays);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IllegalStateException
	 *             {@inheritDoc}
	 * @throws SWTException
	 *             if this method is not invoked from the event-dispatching
	 *             thread
	 * @see #install(Collection)
	 * @see Display#syncExec(Runnable)
	 */
	@Override
	public void uninstall() {
		state.uninstall();
	}

	/**
	 * This method is invoked when a valid transition from uninstalled to
	 * installed is made. It invokes {@link #installFiltersFor(Display)}.
	 * 
	 * @param displays
	 *            the displays to install this strategy to
	 * @see #installFiltersFor(Display)
	 */
	protected void doInstall(final Collection<Display> displays) {
		this.displays = new WeakHashMap<>(displays.size());
		for (final Display display : displays) {
			installFiltersFor(display);
			this.displays.put(display, null);
		}
	}

	/**
	 * Sub-classes implement this method to add {@link Listener}s to the given
	 * display. These listeners should preferably be added as filters so they
	 * are notified early - before other listeners. This method is invoked when
	 * a transition from uninstalled to installed is made.
	 * 
	 * @param display
	 *            the display to install listeners to
	 * @see Display#addFilter(int, Listener)
	 * @see #uninstallFiltersFor(Display)
	 */
	protected abstract void installFiltersFor(final Display display);

	/**
	 * This method reverts the actions performed by
	 * {@link #doInstall(Collection)}. It is invoked only when a transition from
	 * installed to uninstalled is made. It invokes
	 * {@link #uninstallFiltersFor(Display)}.
	 * 
	 * @see #uninstallFiltersFor(Display)
	 */
	protected void doUninstall() {
		displays.forEach((display, nullValue) -> {
			if (!display.isDisposed()) {
				uninstallFiltersFor(display);
			}
		});
		displays.clear();
		displays = null;
	}

	/**
	 * Sub-classes implement this method to revert the actions performed by
	 * their implementation of {@link #installFiltersFor(Display)}. This method
	 * is invoked when a transition from installed to uninstalled is made.
	 * 
	 * @param display
	 *            the display to use
	 * @see #installFiltersFor(Display)
	 */
	protected abstract void uninstallFiltersFor(final Display display);

	private interface State extends Installable {
	}

	private class Uninstalled implements State {

		@Override
		public void install(final Collection<Display> displays) {
			state = new Installed();
			doInstall(displays);
		}

		@Override
		public void uninstall() {
			throw new IllegalStateException("Already un-installed");
		}

	}

	private class Installed implements State {

		@Override
		public void install(final Collection<Display> displays) {
			throw new IllegalArgumentException("Already installed");
		}

		@Override
		public void uninstall() {
			state = new Uninstalled();
			doUninstall();
		}

	}

}
