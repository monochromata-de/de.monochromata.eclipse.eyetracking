package de.monochromata.eclipse.eyetracking.rfl.strategy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionException;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

/**
 * Utility methods for implementing {@link MouseEventCollectionStrategy}
 * sub-classes.
 */
public class StrategyUtils {

	public static Rectangle getBounds(final Menu menu) {
		return syncExec(menu.getDisplay(), () -> {
			System.err.println("menu=" + menu);
			return (Rectangle) accessMethod(menu.getClass(), "getBounds").invoke(menu);
		});
	}

	public static Rectangle getBounds(final MenuItem menuItem) {
		return syncExec(menuItem.getDisplay(), () -> {
			System.err.println("menuItem=" + menuItem);
			return (Rectangle) accessMethod(menuItem.getClass(), "getBounds").invoke(menuItem);
		});
	}

	public static Method accessMethod(final Class<?> clazz, final String methodName, final Class<?>... parameterTypes)
			throws Exception {
		final Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
		method.setAccessible(true);
		return method;
	}

	public static <T> T syncExec(final Display display, final Callable<T> callable) {
		final List<T> result = new ArrayList<T>(1);
		display.syncExec(() -> {
			try {
				result.add(callable.call());
			} catch (final Exception e) {
				throw new CompletionException(e);
			}
		});
		return result.get(0);
	}

	/**
	 * Returns the display-relative bounds of the given control.
	 * 
	 * @param display
	 *            the display containing the control
	 * @param control
	 *            the control whose bounds are to be translated
	 * @return the translated, display-relative bounds of the given control
	 */
	public static Rectangle displayRelativeBounds(final Display display, final Control control) {
		return toDisplay(display, control.getParent(), control.getBounds());
	}

	/**
	 * Maps the given parent-relative bounds of a widget to display-relative
	 * ones.
	 * 
	 * @param display
	 *            the display containing the parent and the widget
	 * @param parent
	 *            the parent-control of the widget
	 * @param bounds
	 *            the bounds of the widget
	 * @return the translated display-relative bounds
	 */
	public static Rectangle toDisplay(final Display display, final Control parent, final Rectangle bounds) {
		return syncExec(display, () -> {
			return display.map(parent, null, bounds);
		});
	}

	// TODO: Use/log monitor-relative coordinates in multi-monitor environments
	// (i.e. when Display has multiple monitors)
	// TODO: Document the relation between monitor and display instances
}
