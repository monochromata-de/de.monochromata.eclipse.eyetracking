package de.monochromata.eclipse.eyetracking.preferences;

import static de.monochromata.eclipse.eyetracking.preferences.PreferenceConstants.ENABLE_EYETRACKING;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.monochromata.eclipse.eyetracking.EyeTracking;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store = EyeTracking.getDefault().getPreferenceStore();
		store.setDefault(ENABLE_EYETRACKING, false);
	}

}
