/**
 * Means of collecting mouse events that may be interpreted as required-fixation
 * locations.
 */
package de.monochromata.eclipse.eyetracking.rfl;