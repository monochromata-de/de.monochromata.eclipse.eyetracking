package de.monochromata.eclipse.eyetracking;

import java.util.function.Consumer;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.EyeTracker;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;

/**
 * TODO: Move to the test jar and make that one available via the p2repo, too.
 * Or write some test utilities jar that downstream plug-ins can use in their
 * tests.
 */
// TODO: Move to de.monochromata.eyetracking/src/main/test
public class TestableEyeTracker extends AbstractTracker<Consumer<Point>> implements EyeTracker {

	/**
	 * Use this method to send a mock gaze event.
	 */
	public void sendGazeEvent(final int x, final int y) {
		final PointImpl gazeEvent = new PointImpl(x, y);
		notifyListeners(listener -> listener.accept(gazeEvent));
	}
}
