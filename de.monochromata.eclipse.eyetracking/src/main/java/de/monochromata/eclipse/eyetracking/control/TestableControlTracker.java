package de.monochromata.eclipse.eyetracking.control;

import java.util.function.BiConsumer;

import org.eclipse.swt.widgets.Control;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;

public class TestableControlTracker extends AbstractTracker<BiConsumer<Point, Control>> implements ControlTracker {

	public void sendControlEvent(final int x, final int y, final Control control) {
		final PointImpl point = new PointImpl(x, y);
		notifyListeners(listener -> listener.accept(point, control));
	}

	@Override
	public void close() throws Exception {
	}

}
