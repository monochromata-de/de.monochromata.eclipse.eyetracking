package de.monochromata.eclipse.eyetracking.control;

import java.util.function.BiConsumer;

import org.eclipse.swt.widgets.Control;

import de.monochromata.eyetracking.EyeTracker;
import de.monochromata.eyetracking.Point;

/**
 * Combines gaze with a control that is at the gaze position.
 * <p>
 * A ControlTracker typically requires an {@link EyeTracker} to function. It
 * will then forward all gaze events for which a control could be assigned to
 * the registered listeners.
 * <p>
 * A ControlTracker shall be closed after use to remove its event listener from
 * the underlying {@link EyeTracker}.
 */
public interface ControlTracker extends AutoCloseable {

	public void addListener(final BiConsumer<Point, Control> listener);

	public void removeListener(final BiConsumer<Point, Control> listener);

	/**
	 * Removes this {@link ControlTracker} from the underlying
	 * {@link EyeTracker}. Note that tracking of the underlying
	 * {@link EyeTracker} will not be stopped by this method.
	 */
	@Override
	void close() throws Exception;

}
