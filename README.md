# An Eclipse Eye-Tracking Plug-in

[![pipeline status](https://gitlab.com/monochromata-de/de.monochromata.eclipse.eyetracking/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/de.monochromata.eclipse.eyetracking/commits/master)

* It is available from a Maven repository at https://monochromata-de.gitlab.io/de.monochromata.eclipse.eyetracking/m2/ .
* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/de.monochromata.eclipse.eyetracking/p2repo/ .

## Links

The project is used by
[de.monochromata.eclipse.anaphors](https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/).

## License

EPL 2.0
